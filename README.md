## 注意：录制saved_waypoints.csv路线时，如果录制的是一个回环路径，起点和终点切勿重合,留有一小节间隙即可
# a星路线转换op路线
1-需要准备录制好的saved_waypoints.csv文件放到主目录下
2-双击运行软件
3-转换好后会在主目录生成一个"4csv"的文件夹，里面有转换好的路线文件

# 加载路径需要注意事项
1-如果Unity里面加载不出路径，那就将4个csv文件在最后删除5排，4个文件要删除相同的排数。然后需要把lane.csv这个文件最后一排的第四格修改为0，保存即可。
2.如果autoware中加载不出路径，先检查dtlane.csv和lane.csv文件排数是否一样，node.csv和point.csv文件排数是否一样，在检查lane.csv这个文件最后一排的第四格是否为0。
